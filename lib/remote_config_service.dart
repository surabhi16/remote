import 'package:firebase_remote_config/firebase_remote_config.dart';

const String _BOOLEAN_VALUE = 'is_remote';

class RemoteConfigService {
  final FirebaseRemoteConfig _remoteConfig;
  RemoteConfigService({required FirebaseRemoteConfig remoteConfig})
      : _remoteConfig = remoteConfig;

  final defaults = <String, dynamic>{
    _BOOLEAN_VALUE: false,
  };

  static RemoteConfigService? _instance;
  static Future<RemoteConfigService> getInstance() async {
    if (_instance == null) {
      _instance = RemoteConfigService(
        remoteConfig: await RemoteConfig.instance,
      );
    }
    return _instance!;
  }

  bool get getBoolValue => _remoteConfig.getBool(_BOOLEAN_VALUE);

  Future initialize() async {
    try {
      await _remoteConfig.setDefaults(defaults);
      await _fetchAndActivate();
    } on NullThrownError catch (e) {
      print("Rmeote Config fetch throttled: $e");
    } catch (e) {
      print("Unable to fetch remote config. Default value will be used");
    }
  }

  Future _fetchAndActivate() async {
    await _remoteConfig.fetch();
    await _remoteConfig.activate();
    await _remoteConfig.setConfigSettings(RemoteConfigSettings(
      fetchTimeout: const Duration(seconds: 1),
      minimumFetchInterval: const Duration(seconds: 1),
    ));
    print("boolean::: $getBoolValue");
  }
}
