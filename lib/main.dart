import 'dart:async';

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remote_config/cubit/resturant_cubit.dart';
import 'package:remote_config/remote_config_service.dart';
import 'package:remote_config/repository/api_reposiitory.dart';
import 'package:remote_config/service/api_service.dart';
import 'UI/remote_config.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  runApp(MyApp(
    apiService: ApiService(),
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key, required this.apiService}) : super(key: key);

  final ApiService apiService;

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<ResturantCubit>(
          create: (context) => ResturantCubit(
            apiRepository: ApiRepository(
              apiService: apiService,
            ),
          )..fetchPostApi(),
        ),
      ],
      child: MaterialApp(
        title: 'Flutter Firebase',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        debugShowCheckedModeBanner: false,
        home: MyHomePage(title: 'Flutter Firebase'),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool isLoading = true;
  RemoteConfigService? _remoteConfigService;
  initializeRemoteConfig() async {
    _remoteConfigService = await RemoteConfigService.getInstance();
    await _remoteConfigService!.initialize();
    setState(() {
      isLoading = false;
    });
  }

  @override
  void initState() {
    Timer(
        Duration(seconds: 5),
        () => Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => RemoteConfig())));
    initializeRemoteConfig();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.black26,
        body: Center(
            child: isLoading
                ? Container(
                    child: Text(
                      "Please Wait",
                      style: TextStyle(color: Colors.white),
                    ),
                  )
                : _remoteConfigService!.getBoolValue
                    ? Container(
                        child: Image.network(
                        "https://purneauniversity.org/wp-content/uploads/2022/12/Merry-Christmas-Images-.png",
                        scale: 0.5,
                      ))
                    : Container(
                        child: Image.network(
                            "https://www.timeanddate.com/scripts/cdog.php?countdownto=Countdown%20to&image=new-year&title=New%20Year&date=Jan%201,%202023&font=cursive"),
                      )));
  }
}
