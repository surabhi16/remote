
class ResturantMenu {
  int? resultsFound;
  int? resultsStart;
  int? resultsShown;
  List<Restaurants>? restaurants;

  ResturantMenu({this.resultsFound, this.resultsStart, this.resultsShown, this.restaurants});

  ResturantMenu.fromJson(Map<String, dynamic> json) {
    if(json["results_found"] is int) {
      resultsFound = json["results_found"];
    }
    if(json["results_start"] is int) {
      resultsStart = json["results_start"];
    }
    if(json["results_shown"] is int) {
      resultsShown = json["results_shown"];
    }
    if(json["restaurants"] is List) {
      restaurants = json["restaurants"] == null ? null : (json["restaurants"] as List).map((e) => Restaurants.fromJson(e)).toList();
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["results_found"] = resultsFound;
    _data["results_start"] = resultsStart;
    _data["results_shown"] = resultsShown;
    if(restaurants != null) {
      _data["restaurants"] = restaurants?.map((e) => e.toJson()).toList();
    }
    return _data;
  }
}

class Restaurants {
  Restaurant? restaurant;

  Restaurants({this.restaurant});

  Restaurants.fromJson(Map<String, dynamic> json) {
    if(json["restaurant"] is Map) {
      restaurant = json["restaurant"] == null ? null : Restaurant.fromJson(json["restaurant"]);
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    if(restaurant != null) {
      _data["restaurant"] = restaurant?.toJson();
    }
    return _data;
  }
}

class Restaurant {
  R? r;
  String? apikey;
  String? id;
  String? name;
  String? url;
  Location? location;
  int? switchToOrderMenu;
  String? cuisines;
  String? timings;
  int? averageCostForTwo;
  int? priceRange;
  String? currency;
  List<String>? highlights;
  List<dynamic>? offers;
  int? opentableSupport;
  int? isZomatoBookRes;
  String? mezzoProvider;
  int? isBookFormWebView;
  String? bookFormWebViewUrl;
  String? bookAgainUrl;
  String? thumb;
  UserRating? userRating;
  int? allReviewsCount;
  String? photosUrl;
  int? photoCount;
  String? menuUrl;
  String? featuredImage;
  bool? medioProvider;
  int? hasOnlineDelivery;
  int? isDeliveringNow;
  String? storeType;
  bool? includeBogoOffers;
  String? deeplink;
  int? isTableReservationSupported;
  int? hasTableBooking;
  String? bookUrl;
  String? eventsUrl;
  String? phoneNumbers;
  AllReviews? allReviews;
  List<String>? establishment;
  List<dynamic>? establishmentTypes;

  Restaurant({this.r, this.apikey, this.id, this.name, this.url, this.location, this.switchToOrderMenu, this.cuisines, this.timings, this.averageCostForTwo, this.priceRange, this.currency, this.highlights, this.offers, this.opentableSupport, this.isZomatoBookRes, this.mezzoProvider, this.isBookFormWebView, this.bookFormWebViewUrl, this.bookAgainUrl, this.thumb, this.userRating, this.allReviewsCount, this.photosUrl, this.photoCount, this.menuUrl, this.featuredImage, this.medioProvider, this.hasOnlineDelivery, this.isDeliveringNow, this.storeType, this.includeBogoOffers, this.deeplink, this.isTableReservationSupported, this.hasTableBooking, this.bookUrl, this.eventsUrl, this.phoneNumbers, this.allReviews, this.establishment, this.establishmentTypes});

  Restaurant.fromJson(Map<String, dynamic> json) {
    if(json["R"] is Map) {
      r = json["R"] == null ? null : R.fromJson(json["R"]);
    }
    if(json["apikey"] is String) {
      apikey = json["apikey"];
    }
    if(json["id"] is String) {
      id = json["id"];
    }
    if(json["name"] is String) {
      name = json["name"];
    }
    if(json["url"] is String) {
      url = json["url"];
    }
    if(json["location"] is Map) {
      location = json["location"] == null ? null : Location.fromJson(json["location"]);
    }
    if(json["switch_to_order_menu"] is int) {
      switchToOrderMenu = json["switch_to_order_menu"];
    }
    if(json["cuisines"] is String) {
      cuisines = json["cuisines"];
    }
    if(json["timings"] is String) {
      timings = json["timings"];
    }
    if(json["average_cost_for_two"] is int) {
      averageCostForTwo = json["average_cost_for_two"];
    }
    if(json["price_range"] is int) {
      priceRange = json["price_range"];
    }
    if(json["currency"] is String) {
      currency = json["currency"];
    }
    if(json["highlights"] is List) {
      highlights = json["highlights"] == null ? null : List<String>.from(json["highlights"]);
    }
    if(json["offers"] is List) {
      offers = json["offers"] ?? [];
    }
    if(json["opentable_support"] is int) {
      opentableSupport = json["opentable_support"];
    }
    if(json["is_zomato_book_res"] is int) {
      isZomatoBookRes = json["is_zomato_book_res"];
    }
    if(json["mezzo_provider"] is String) {
      mezzoProvider = json["mezzo_provider"];
    }
    if(json["is_book_form_web_view"] is int) {
      isBookFormWebView = json["is_book_form_web_view"];
    }
    if(json["book_form_web_view_url"] is String) {
      bookFormWebViewUrl = json["book_form_web_view_url"];
    }
    if(json["book_again_url"] is String) {
      bookAgainUrl = json["book_again_url"];
    }
    if(json["thumb"] is String) {
      thumb = json["thumb"];
    }
    if(json["user_rating"] is Map) {
      userRating = json["user_rating"] == null ? null : UserRating.fromJson(json["user_rating"]);
    }
    if(json["all_reviews_count"] is int) {
      allReviewsCount = json["all_reviews_count"];
    }
    if(json["photos_url"] is String) {
      photosUrl = json["photos_url"];
    }
    if(json["photo_count"] is int) {
      photoCount = json["photo_count"];
    }
    if(json["menu_url"] is String) {
      menuUrl = json["menu_url"];
    }
    if(json["featured_image"] is String) {
      featuredImage = json["featured_image"];
    }
    if(json["medio_provider"] is bool) {
      medioProvider = json["medio_provider"];
    }
    if(json["has_online_delivery"] is int) {
      hasOnlineDelivery = json["has_online_delivery"];
    }
    if(json["is_delivering_now"] is int) {
      isDeliveringNow = json["is_delivering_now"];
    }
    if(json["store_type"] is String) {
      storeType = json["store_type"];
    }
    if(json["include_bogo_offers"] is bool) {
      includeBogoOffers = json["include_bogo_offers"];
    }
    if(json["deeplink"] is String) {
      deeplink = json["deeplink"];
    }
    if(json["is_table_reservation_supported"] is int) {
      isTableReservationSupported = json["is_table_reservation_supported"];
    }
    if(json["has_table_booking"] is int) {
      hasTableBooking = json["has_table_booking"];
    }
    if(json["book_url"] is String) {
      bookUrl = json["book_url"];
    }
    if(json["events_url"] is String) {
      eventsUrl = json["events_url"];
    }
    if(json["phone_numbers"] is String) {
      phoneNumbers = json["phone_numbers"];
    }
    if(json["all_reviews"] is Map) {
      allReviews = json["all_reviews"] == null ? null : AllReviews.fromJson(json["all_reviews"]);
    }
    if(json["establishment"] is List) {
      establishment = json["establishment"] == null ? null : List<String>.from(json["establishment"]);
    }
    if(json["establishment_types"] is List) {
      establishmentTypes = json["establishment_types"] ?? [];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    if(r != null) {
      _data["R"] = r?.toJson();
    }
    _data["apikey"] = apikey;
    _data["id"] = id;
    _data["name"] = name;
    _data["url"] = url;
    if(location != null) {
      _data["location"] = location?.toJson();
    }
    _data["switch_to_order_menu"] = switchToOrderMenu;
    _data["cuisines"] = cuisines;
    _data["timings"] = timings;
    _data["average_cost_for_two"] = averageCostForTwo;
    _data["price_range"] = priceRange;
    _data["currency"] = currency;
    if(highlights != null) {
      _data["highlights"] = highlights;
    }
    if(offers != null) {
      _data["offers"] = offers;
    }
    _data["opentable_support"] = opentableSupport;
    _data["is_zomato_book_res"] = isZomatoBookRes;
    _data["mezzo_provider"] = mezzoProvider;
    _data["is_book_form_web_view"] = isBookFormWebView;
    _data["book_form_web_view_url"] = bookFormWebViewUrl;
    _data["book_again_url"] = bookAgainUrl;
    _data["thumb"] = thumb;
    if(userRating != null) {
      _data["user_rating"] = userRating?.toJson();
    }
    _data["all_reviews_count"] = allReviewsCount;
    _data["photos_url"] = photosUrl;
    _data["photo_count"] = photoCount;
    _data["menu_url"] = menuUrl;
    _data["featured_image"] = featuredImage;
    _data["medio_provider"] = medioProvider;
    _data["has_online_delivery"] = hasOnlineDelivery;
    _data["is_delivering_now"] = isDeliveringNow;
    _data["store_type"] = storeType;
    _data["include_bogo_offers"] = includeBogoOffers;
    _data["deeplink"] = deeplink;
    _data["is_table_reservation_supported"] = isTableReservationSupported;
    _data["has_table_booking"] = hasTableBooking;
    _data["book_url"] = bookUrl;
    _data["events_url"] = eventsUrl;
    _data["phone_numbers"] = phoneNumbers;
    if(allReviews != null) {
      _data["all_reviews"] = allReviews?.toJson();
    }
    if(establishment != null) {
      _data["establishment"] = establishment;
    }
    if(establishmentTypes != null) {
      _data["establishment_types"] = establishmentTypes;
    }
    return _data;
  }
}

class AllReviews {
  List<Reviews>? reviews;

  AllReviews({this.reviews});

  AllReviews.fromJson(Map<String, dynamic> json) {
    if(json["reviews"] is List) {
      reviews = json["reviews"] == null ? null : (json["reviews"] as List).map((e) => Reviews.fromJson(e)).toList();
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    if(reviews != null) {
      _data["reviews"] = reviews?.map((e) => e.toJson()).toList();
    }
    return _data;
  }
}

class Reviews {
  List<dynamic>? review;

  Reviews({this.review});

  Reviews.fromJson(Map<String, dynamic> json) {
    if(json["review"] is List) {
      review = json["review"] ?? [];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    if(review != null) {
      _data["review"] = review;
    }
    return _data;
  }
}

class UserRating {
  String? aggregateRating;
  String? ratingText;
  String? ratingColor;
  RatingObj? ratingObj;
  int? votes;

  UserRating({this.aggregateRating, this.ratingText, this.ratingColor, this.ratingObj, this.votes});

  UserRating.fromJson(Map<String, dynamic> json) {
    if(json["aggregate_rating"] is String) {
      aggregateRating = json["aggregate_rating"];
    }
    if(json["rating_text"] is String) {
      ratingText = json["rating_text"];
    }
    if(json["rating_color"] is String) {
      ratingColor = json["rating_color"];
    }
    if(json["rating_obj"] is Map) {
      ratingObj = json["rating_obj"] == null ? null : RatingObj.fromJson(json["rating_obj"]);
    }
    if(json["votes"] is int) {
      votes = json["votes"];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["aggregate_rating"] = aggregateRating;
    _data["rating_text"] = ratingText;
    _data["rating_color"] = ratingColor;
    if(ratingObj != null) {
      _data["rating_obj"] = ratingObj?.toJson();
    }
    _data["votes"] = votes;
    return _data;
  }
}

class RatingObj {
  Title? title;
  BgColor? bgColor;

  RatingObj({this.title, this.bgColor});

  RatingObj.fromJson(Map<String, dynamic> json) {
    if(json["title"] is Map) {
      title = json["title"] == null ? null : Title.fromJson(json["title"]);
    }
    if(json["bg_color"] is Map) {
      bgColor = json["bg_color"] == null ? null : BgColor.fromJson(json["bg_color"]);
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    if(title != null) {
      _data["title"] = title?.toJson();
    }
    if(bgColor != null) {
      _data["bg_color"] = bgColor?.toJson();
    }
    return _data;
  }
}

class BgColor {
  String? type;
  String? tint;

  BgColor({this.type, this.tint});

  BgColor.fromJson(Map<String, dynamic> json) {
    if(json["type"] is String) {
      type = json["type"];
    }
    if(json["tint"] is String) {
      tint = json["tint"];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["type"] = type;
    _data["tint"] = tint;
    return _data;
  }
}

class Title {
  String? text;

  Title({this.text});

  Title.fromJson(Map<String, dynamic> json) {
    if(json["text"] is String) {
      text = json["text"];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["text"] = text;
    return _data;
  }
}

class Location {
  String? address;
  String? locality;
  String? city;
  int? cityId;
  String? latitude;
  String? longitude;
  String? zipcode;
  int? countryId;
  String? localityVerbose;

  Location({this.address, this.locality, this.city, this.cityId, this.latitude, this.longitude, this.zipcode, this.countryId, this.localityVerbose});

  Location.fromJson(Map<String, dynamic> json) {
    if(json["address"] is String) {
      address = json["address"];
    }
    if(json["locality"] is String) {
      locality = json["locality"];
    }
    if(json["city"] is String) {
      city = json["city"];
    }
    if(json["city_id"] is int) {
      cityId = json["city_id"];
    }
    if(json["latitude"] is String) {
      latitude = json["latitude"];
    }
    if(json["longitude"] is String) {
      longitude = json["longitude"];
    }
    if(json["zipcode"] is String) {
      zipcode = json["zipcode"];
    }
    if(json["country_id"] is int) {
      countryId = json["country_id"];
    }
    if(json["locality_verbose"] is String) {
      localityVerbose = json["locality_verbose"];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["address"] = address;
    _data["locality"] = locality;
    _data["city"] = city;
    _data["city_id"] = cityId;
    _data["latitude"] = latitude;
    _data["longitude"] = longitude;
    _data["zipcode"] = zipcode;
    _data["country_id"] = countryId;
    _data["locality_verbose"] = localityVerbose;
    return _data;
  }
}

class R {
  int? resId;
  bool? isGroceryStore;
  HasMenuStatus? hasMenuStatus;

  R({this.resId, this.isGroceryStore, this.hasMenuStatus});

  R.fromJson(Map<String, dynamic> json) {
    if(json["res_id"] is int) {
      resId = json["res_id"];
    }
    if(json["is_grocery_store"] is bool) {
      isGroceryStore = json["is_grocery_store"];
    }
    if(json["has_menu_status"] is Map) {
      hasMenuStatus = json["has_menu_status"] == null ? null : HasMenuStatus.fromJson(json["has_menu_status"]);
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["res_id"] = resId;
    _data["is_grocery_store"] = isGroceryStore;
    if(hasMenuStatus != null) {
      _data["has_menu_status"] = hasMenuStatus?.toJson();
    }
    return _data;
  }
}

class HasMenuStatus {
  int? delivery;
  int? takeaway;

  HasMenuStatus({this.delivery, this.takeaway});

  HasMenuStatus.fromJson(Map<String, dynamic> json) {
    if(json["delivery"] is int) {
      delivery = json["delivery"];
    }
    if(json["takeaway"] is int) {
      takeaway = json["takeaway"];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["delivery"] = delivery;
    _data["takeaway"] = takeaway;
    return _data;
  }
}