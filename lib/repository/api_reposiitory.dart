import 'dart:convert';

import 'package:remote_config/model/resturant_menu.dart';

import '../service/api_service.dart';

class ApiRepository {
  const ApiRepository({
    required this.apiService,
  });
  final ApiService apiService;

  Future<List<Restaurants>?> getPostList() async {
    final response = await apiService.getPostData();
    if (response != null) {
      Map<String, dynamic> map = json.decode(response.data);

      List<dynamic> data = map["restaurants"];

      print("myList===" + data.length.toString());
      return data.map((e) => Restaurants.fromJson(e)).toList();
    }
  }
}
