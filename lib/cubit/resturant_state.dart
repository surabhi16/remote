part of 'resturant_cubit.dart';

@immutable
abstract class ResturantState extends Equatable {
  const ResturantState();
  @override
  List<Object> get props => [];
}

class ResturantInitial extends ResturantState {}

class ResturnatFetchLoading extends ResturantState {}

class ResturantFetchLoaded extends ResturantState {
  final List<Restaurants> resturantList;

  const ResturantFetchLoaded({
    required this.resturantList,
  });

  @override
  List<Object> get props => [resturantList];
}

class ResturantFetchError extends ResturantState {
  final Failure failure;

  const ResturantFetchError({
    required this.failure,
  });
  @override
  List<Object> get props => [failure];
}
