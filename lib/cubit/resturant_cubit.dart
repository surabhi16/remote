import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:remote_config/model/resturant_menu.dart';

import '../model/failure_model.dart';
import '../repository/api_reposiitory.dart';

part 'resturant_state.dart';

class ResturantCubit extends Cubit<ResturantState> {
  final ApiRepository apiRepository;

  ResturantCubit({required this.apiRepository}) : super(ResturantInitial());

  Future<void> fetchPostApi() async {
    emit(ResturnatFetchLoading());
    try {
      final List<Restaurants>? postList = await apiRepository.getPostList();
      emit(ResturantFetchLoaded(resturantList: postList ?? []));
    } on Failure catch (err) {
      emit(ResturantFetchError(failure: err));
    } catch (err) {
      print("Error :$err");
    }
  }
}
