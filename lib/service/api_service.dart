import 'dart:io';

import 'package:dio/dio.dart';

import '../model/failure_model.dart';

class ApiService {
  final Dio _dio = Dio();

  Future<Response?> getPostData() async {
    try {
      final Response? response = await _dio.get(
          'https://raw.githubusercontent.com/AustinRay123/dummyjSON/main/dummyProduct.json');

      // print("getres==>" + response.toString());
      return response;
    } on SocketException {
      throw const Failure(message: 'No Internet Connection');
    } catch (err) {
      print("Error :$err");
    }
  }
}
