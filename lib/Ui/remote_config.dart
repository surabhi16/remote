import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../cubit/resturant_cubit.dart';
import '../remote_config_service.dart';

class RemoteConfig extends StatefulWidget {
  @override
  _RemoteConfigState createState() => _RemoteConfigState();
}

class _RemoteConfigState extends State<RemoteConfig> {
  bool isLoading = true;

  final List<Map<String, String>> popularFood = [
    {
      'name': 'Tandoori Chicken',
      'price': '96.00',
      'rate': '4.9',
      'clients': '200',
      'image': 'images/plate-001.png'
    },
    {
      'name': 'Salmon',
      'price': '40.50',
      'rate': '4.5',
      'clients': '168',
      'image': 'images/plate-002.png'
    },
    {
      'name': 'Rice and meat',
      'price': '130.00',
      'rate': '4.8',
      'clients': '150',
      'image': 'images/plate-003.png'
    },
    {
      'name': 'Vegan food',
      'price': '400.00',
      'rate': '4.2',
      'clients': '150',
      'image': 'images/plate-007.png'
    },
    {
      'name': 'Rich food',
      'price': '1000.00',
      'rate': '4.6',
      'clients': '10',
      'image': 'images/plate-006.png'
    }
  ];

  final List<Map<String, String>> foodOptions = [
    {
      'name': 'Proteins',
      'image': 'images/Icon-001.png',
    },
    {
      'name': 'Burger',
      'image': 'images/Icon-002.png',
    },
    {
      'name': 'Fastfood',
      'image': 'images/Icon-003.png',
    },
    {
      'name': 'Salads',
      'image': 'images/Icon-004.png',
    }
  ];
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ThemeData theme = Theme.of(context);
    Size size = MediaQuery.of(context).size;
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.black),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: Text("Flutter Firebase"),
          centerTitle: true,
        ),
        body: BlocBuilder<ResturantCubit, ResturantState>(
          builder: (context, state) {
            if (state is ResturnatFetchLoading) {
              return Center(child: const CircularProgressIndicator());
            } else if (state is ResturantFetchLoaded) {
              final menulist = state.resturantList;
              return ListView.builder(
                  itemCount: menulist.length,
                  itemBuilder: ((context, index) {
                    return GestureDetector(
                        onTap: () {},
                        child: Hero(
                          tag: 'detail_food${this.popularFood.length}',
                          child: Card(
                            //  width: size.width / 2,
                            color: Colors.white,
                            //   padding: const EdgeInsets.only(bottom: 8.0),
                            margin: const EdgeInsets.only(
                                left: 5.0, right: 5.0, bottom: 5.0, top: 10),
                            child: Column(
                              children: <Widget>[
                                Stack(
                                  children: <Widget>[
                                    Container(
                                      height: size.height / 4,
                                      width: size.width * 2,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(5.0),
                                        ),
                                        image: DecorationImage(
                                          fit: BoxFit.cover,
                                          image: NetworkImage(
                                              "https://b.zmtcdn.com/data/pictures/6/19310366/66c2dafb6e6f39d859b928090bad44ac.jpg?output-format=webp&fit=around|771.75:416.25&crop=771.75:416.25;*,*"),
                                        ),
                                      ),
                                    ),
                                    Container(
                                      alignment: Alignment.topRight,
                                      margin: const EdgeInsets.all(8.0),
                                      child: Container(
                                        padding: const EdgeInsets.all(8.0),
                                        decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: Colors.white,
                                        ),
                                        child: Icon(
                                          Icons.favorite,
                                          size: 25.0,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: Positioned(
                                        // The Positioned widget is used to position the text inside the Stack widget
                                        bottom: 10,
                                        //right: 10,

                                        child: Container(
                                          // We use this Container to create a black box that wraps the white text so that the user can read the text even when the image is white

                                          color: Colors.blue,
                                          padding: const EdgeInsets.all(8),
                                          child: Text(
                                            menulist[index]
                                                        .restaurant!
                                                        .offers!
                                                        .length >
                                                    0
                                                ? "1"
                                                : (index + 5).toString() +
                                                    " %off",
                                            style: TextStyle(
                                                fontSize: 12,
                                                color: Colors.white),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: Positioned(
                                        // The Positioned widget is used to position the text inside the Stack widget
                                        bottom: 10,
                                        right: 10,

                                        child: Container(
                                          // We use this Container to create a black box that wraps the white text so that the user can read the text even when the image is white
                                          // width: 300,
                                          padding: const EdgeInsets.all(5),
                                          decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(5.0))),
                                          child: Row(
                                            children: [
                                              Icon(
                                                Icons.timelapse,
                                                size: 10,
                                              ),
                                              Text(
                                                (index + 10).toString() +
                                                    " min",
                                                style: TextStyle(
                                                    fontSize: 10,
                                                    color: Colors.black),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                    top: 5.0,
                                    bottom: 0.0,
                                    left: 10.0,
                                    right: 10.0,
                                  ),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        menulist[index]
                                            .restaurant!
                                            .name
                                            .toString(),
                                        style: TextStyle(
                                            fontSize: 18.0,
                                            color: Colors.black,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      Container(
                                          padding: const EdgeInsets.all(4.0),
                                          decoration: BoxDecoration(
                                              color: Colors.green,
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(5.0))),
                                          child: Row(
                                            children: [
                                              Text(
                                                menulist[index]
                                                    .restaurant!
                                                    .userRating!
                                                    .aggregateRating
                                                    .toString(),
                                                style: TextStyle(
                                                    color: Colors.white),
                                              ),
                                              Icon(
                                                Icons.star,
                                                size: 10,
                                                color: Colors.white,
                                              )
                                            ],
                                          )),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                    top: 5.0,
                                    left: 10.0,
                                    right: 10.0,
                                  ),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Expanded(
                                        child: Text(
                                          menulist[index]
                                              .restaurant!
                                              .cuisines
                                              .toString(),
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                          style: TextStyle(
                                            fontSize: 16.0,
                                            color: Colors.black,
                                          ),
                                        ),
                                      ),
                                      Container(
                                        child: Text(
                                          menulist[index]
                                                  .restaurant!
                                                  .currency
                                                  .toString() +
                                              menulist[index]
                                                  .restaurant!
                                                  .averageCostForTwo
                                                  .toString() +
                                              " for two person",
                                          style: TextStyle(
                                            fontSize: 16.0,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(5.0),
                                  child: Divider(
                                    thickness: 1,
                                    height: 5,
                                  ),
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
                                      alignment: Alignment.topRight,
                                      margin: const EdgeInsets.all(5.0),
                                      child: Container(
                                        padding: const EdgeInsets.all(5.0),
                                        decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: Colors.purple,
                                        ),
                                        child: Icon(
                                          Icons.auto_graph,
                                          size: 12.0,
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                    Text(
                                      menulist[index]
                                          .restaurant!
                                          .userRating!
                                          .votes
                                          .toString(),
                                      style: TextStyle(
                                        fontSize: 12.0,
                                        color: Colors.grey,
                                      ),
                                    ),
                                    Text(
                                      ' order placed recently from here',
                                      style: TextStyle(
                                        fontSize: 12.0,
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ));
                  }));
            }
            return const SizedBox.shrink();
          },
        ));
  }
}
